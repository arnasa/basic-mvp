package com.example.arnas.mvp.main

import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import com.example.arnas.mvp.R

class MainActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        supportFragmentManager.beginTransaction()
                .replace(R.id.fragmentContainer, MainFragment())
                .commit()
    }
}
