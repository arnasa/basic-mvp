package com.example.arnas.mvp.main

import io.reactivex.Observable

interface MainContract {
    interface View {
        fun showData(data: String)

        fun showError(error: String)
    }

    interface Presenter {
        fun takeView(view: View)

        fun dropView()

        fun onViewStared()
    }

    interface Model {
        fun loadData(): Observable<String>
    }
}