package com.example.arnas.mvp.main

import android.os.Bundle
import android.support.v4.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.example.arnas.mvp.R
import com.example.arnas.mvp.network.FakeServiceFactory
import io.reactivex.android.schedulers.AndroidSchedulers
import kotlinx.android.synthetic.main.fragment_main.*

class MainFragment: Fragment(), MainContract.View{
    private lateinit var presenter: MainContract.Presenter

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        val api = FakeServiceFactory.createApi()
        val model = MainModel(api)
        presenter = MainPresenter(model, AndroidSchedulers.mainThread())
        presenter.takeView(this)
    }

    override fun onDestroyView() {
        presenter.dropView()
        super.onDestroyView()
    }

    override fun onCreateView(
            inflater: LayoutInflater,
            container: ViewGroup?,
            savedInstanceState: Bundle?
    ): View? = inflater.inflate(R.layout.fragment_main, container, false)

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        presenter.onViewStared()
    }

    override fun showData(data: String) {
        someTextView.text = data
    }

    override fun showError(error: String) {
        someTextView.text = error
    }
}