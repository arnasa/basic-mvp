package com.example.arnas.mvp.main

import com.example.arnas.mvp.network.SomeApi
import io.reactivex.Observable

class MainModel(private val service: SomeApi) : MainContract.Model {

    override fun loadData(): Observable<String> {
        return service.getData()
    }
}