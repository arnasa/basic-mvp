package com.example.arnas.mvp.main

import io.reactivex.Scheduler

class MainPresenter(
        private val model: MainContract.Model,
        private val uiScheduler: Scheduler
) : MainContract.Presenter {

    private var view: MainContract.View? = null

    override fun takeView(view: MainContract.View) {
        this.view = view
    }

    override fun dropView() {
        view = null
    }

    override fun onViewStared() {
        model.loadData()
                .observeOn(uiScheduler)
                .subscribe(
                        { data -> view?.showData(data) },
                        { error -> view?.showError(error.message.toString()) })
    }
}