package com.example.arnas.mvp.network

import io.reactivex.Observable

object FakeServiceFactory {
    fun createApi(): SomeApi {
        return object : SomeApi {
            override fun getData(): Observable<String> {
                return Observable.just("This is loaded data")
            }
        }
    }
}