package com.example.arnas.mvp.network

import io.reactivex.Observable
import retrofit2.http.GET

interface SomeApi {
    @GET("url")
    fun getData(): Observable<String>
}