package com.example.arnas.mvp.main

import com.example.arnas.mvp.network.SomeApi
import com.nhaarman.mockito_kotlin.given
import com.nhaarman.mockito_kotlin.mock
import io.reactivex.Observable
import org.junit.Before
import org.junit.Test

class MainModelTest {
    private val api = mock<SomeApi>()
    private val mainModel = MainModel(api)

    @Before
    fun setup(){

    }

    @Test
    fun loadData_apiSuccess_dataIsReturned() {
        val expectedValue = "this is expected"
        given { api.getData() }.willReturn(Observable.just(expectedValue))

        val observer = mainModel.loadData().test()

        observer.assertValue(expectedValue)
    }

    @Test
    fun loadData_apiError_exceptionIsThrown(){
        val expectedError = RuntimeException("Error")
        given { api.getData() }.willReturn(Observable.error(expectedError))

        val observer = mainModel.loadData().test()

        observer.assertError(expectedError)
    }

}