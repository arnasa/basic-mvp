package com.example.arnas.mvp.main

import com.nhaarman.mockito_kotlin.*
import io.reactivex.Observable
import io.reactivex.schedulers.TestScheduler
import org.junit.Before
import org.junit.Test

class MainPresenterTest {
    private val model = mock<MainContract.Model>()
    private val view = mock<MainContract.View>()
    private val standardValue = "Some Value"
    private val scheduler = TestScheduler()
    private val presenter = MainPresenter(model, scheduler)

    @Before
    fun setup() {
        given { model.loadData() }.willReturn(Observable.just(standardValue))
    }

    @Test
    fun onViewStarted_modelLoadDataIsCalled() {
        presenter.onViewStared()

        verify(model).loadData()
    }

    @Test
    fun onViewStarted_viewIsSet_valuePassedToView() {
        presenter.takeView(view)

        presenter.onViewStared()
        scheduler.triggerActions()

        verify(view).showData(standardValue)
    }

    @Test
    fun onViewStarted_viewIsNotSet_noInteractionOnView() {
        presenter.onViewStared()
        scheduler.triggerActions()

        verifyNoMoreInteractions(view)
    }

    @Test
    fun onViewStarted_viewIsDropped_noValueIsPassedToView() {
        presenter.takeView(view)
        presenter.dropView()

        presenter.onViewStared()
        scheduler.triggerActions()

        verifyNoMoreInteractions(view)
    }

    @Test
    fun onViewStarted_errorIsThrownInModel_errorIsShown() {
        val message = "Error message"
        given { model.loadData() }.willReturn(Observable.error(RuntimeException(message)))

        presenter.takeView(view)
        presenter.onViewStared()
        scheduler.triggerActions()

        verify(view).showError(message)
    }

    @Test
    fun onViewStarted_errorIsThrownInModel_viewIsNotSet_noInteractionWithView() {
        val message = "Error message"
        given { model.loadData() }.willReturn(Observable.error(RuntimeException(message)))

        presenter.onViewStared()
        scheduler.triggerActions()

        verifyZeroInteractions(view)
    }

    @Test
    fun onViewStarted_errorIsThrownInModel_viewIsDropped_noInteractionWithView() {
        val message = "Error message"
        given { model.loadData() }.willReturn(Observable.error(RuntimeException(message)))

        presenter.takeView(view)
        presenter.dropView()
        presenter.onViewStared()
        scheduler.triggerActions()

        verifyZeroInteractions(view)
    }
}